/// <reference types="cypress" />

describe('Privy', () => {
        it('Login dasboard', () => {
            //boostraping external
            cy.viewport(1000, 660)
            cy.visit('https://oauth.privy.id/')

        //login
        cy.get('[name="user[privyId]"]').type('UAT001{enter}',{force: true})
        cy.wait(5000) //memberi jeda waktu agar tidak terjadi bug
        cy.get('[required="required"]').type('Akuntes1{enter}')


        cy.wait(9000)
        cy.get('#v-step-0').click()
        cy.wait(600)
        cy.get(':nth-child(1) > .workflow > .workflow__label').click()


        cy.visit('https://app.privy.id/upload/self-sign/upload')
        const fileName = 'Bab III.pdf';
        
        // upload file PDF
        cy.wait(500)
        cy.fixture(fileName, 'binary')
        .then(Cypress.Blob.binaryStringToBlob)
        .then(fileContent => {
            cy.get('[type="file"]').attachFile({
                fileContent,
                fileName,
                mimeType: 'application/pdf',
                encoding: 'utf8',
                lastModified: new Date().getTime('.btn-danger'),

        
            
        })
        //ttd document
        cy.wait(500)
        cy.get('[class="btn btn-danger"]').click()

        cy.wait(9000)
        cy.get('[class="btn btn-danger mx-2"]').click()

        cy.wait(7000)
        cy.get('[class="btn btn-success mx-2 my-2"]').click()

        cy.wait(6000)
        cy.get('.justify-content-end > .btn-danger').click()

        cy.wait(10000)
        cy.get('[id="form-sign"] > fieldset:nth-child(2) > div > div.form-otp > div:nth-child(1) > input:nth-child(1)').type(12345)


        // cy.wait(6000)
        // cy.get('type="tel"').type('12345');

        cy.wait(6000)
        cy.get('[class="btn btn-danger btn-center px-4"]').click()


    //     cy.wait(800)
    //   cy.get('#v-step-0')
    //   cy.wait(700)
    //   cy.get('[class="workflow"]')

        cy.wait(7000)
      cy.get('#v-menus-0').click()

    
      })
      

    //   cy.visit('https://app.privy.id/upload/Sign & Request/upload')
    //   const fileName = 'Bab III.pdf';

    //   cy.wait(500)
    //     cy.fixture(fileName, 'binary')
    //     .then(Cypress.Blob.binaryStringToBlob)
    //     .then(fileContent => {
    //         cy.get('[type="file"]').attachFile({
    //             fileContent,
    //             fileName,
    //             mimeType: 'application/pdf',
    //             encoding: 'utf8',
    //             lastModified: new Date().getTime('.btn-danger'),
    //         })
    //     })

    })

})